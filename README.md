# NetBeans Bash-completion
A simple Bash completion script for the NetBeans IDE (the `netbeans` command).

## Installation
Install it for example by `source`ing the `netbeans` script in your `~/.bashrc`
file. For system-wide installation, consult documentation of your distribution
(usually, creating a symlink into `/etc/bash_completion.d` directory should
work).

_Note:_ A temporary file `/tmp/nb-bashcompletion` is created and used for
better performance. This should be OK since the `/tmp` directory is usually
cleaned on every system startup.
